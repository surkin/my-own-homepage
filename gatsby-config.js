module.exports = {
  pathPrefix: `/gatsby`,
  siteMetadata: {
    title: `Gatsby surkin Starter`,
  },
  plugins: [`gatsby-plugin-react-helmet`],
}
